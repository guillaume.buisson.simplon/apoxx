import sprite from '../assets/images/sprite2D/ennemies/zombie.gif';
import explosion from '../assets/images/sprite2D/environnement/explosion_zombie.gif';
import ammoBox from '../assets/images/sprite2D/environnement/ammoPickup.png';
import healthBox from '../assets/images/sprite2D/environnement/HealthPickup.gif';
import {WaveManager} from "./WaveManager";

export class Zombie {
    constructor() {
        this.width = '80px';
        this.heigth = '80px';
        this.sprite = sprite;
        this.deathAnim = explosion;
        this.ammoBox = ammoBox;
        this.healthBox = healthBox;
        this.speed = 2;
        this.life = 100;
        this.maxlife = 100;
        this.damage = 10;
    }

    zombieDead() {
        this.explosion = document.createElement('div');
        this.explosion.style.width = '80px';
        this.explosion.style.height = '80px';
        this.explosion.style.position = 'absolute';
        this.explosion.style.top = '0';
        this.explosion.style.left = '0';
        this.explosion.style.backgroundImage = `url('${this.deathAnim}')`;
        this.explosion.style.backgroundRepeat = 'no-repeat';
        this.explosion.style.backgroundSize = 'cover';

        return this.explosion
    }

    makeZombie(X, Y) {
        const div = document.getElementById('zombie-section');

        this.zombie = document.createElement('div');
        this.zombie.setAttribute('class', 'zombie')
        this.zombie.style.width = this.width;
        this.zombie.style.height = this.heigth;
        this.zombie.style.backgroundImage = `url('${this.sprite}')`;
        this.zombie.style.backgroundRepeat = 'no-repeat';
        this.zombie.style.backgroundSize = 'contain';
        this.zombie.style.visibility = 'hidden';
        this.zombie.style.position = 'absolute';
        this.zombie.style.top = `${X}px`;
        this.zombie.style.left = `${Y}px`;

        div.append(this.zombie);
        this.makeHUD();
    }

    makeHUD() {
        this.zombieLife = document.createElement('progress');
        this.zombieLife.value = this.life;
        this.zombieLife.max = this.maxlife;
        this.zombieLife.style.width = '80%';
        this.zombieLife.style.position = 'absolute';
        this.zombieLife.style.top = '0';
        this.zombieLife.style.left = '0';

        this.zombie.append(this.zombieLife);
    }

    followPlayer(zombie) {
        const playerObj = document.getElementById('camera');

        if(zombie.getBoundingClientRect()) {
            setInterval(() => {
                let offset = zombie.getBoundingClientRect();
                let center_x = (offset.left) + (parseInt(this.width)/2);
                let center_y = (offset.top) + (parseInt(this.heigth)/2);
                let player_x = playerObj.offsetLeft + (parseInt(playerObj.style.width)/2),
                    player_y = playerObj.offsetTop + (parseInt(playerObj.style.height)/2);
                let radians = Math.atan2(player_x - center_x, player_y - center_y);
                let degree = (radians * (180 / Math.PI) * -1) + 90;

                zombie.style.transform = `rotate(${degree}deg)`;
            }, 60)
        }
    }

    spawnZombies(nbr) {
        for (let i = 0; i < nbr; i++) {
            let random_x = Math.floor(Math.random() * window.innerWidth),
                random_y = Math.floor(Math.random() * window.innerHeight)
            this.makeZombie(random_x, random_y);
        }
    }

    walk(player, zombie) {
        const playerObj = document.getElementById('camera');
        const bloodEffect = document.getElementById('bloodEffect');

        setInterval(() => {
            if(zombie.getBoundingClientRect()) {
                let offset = zombie.getBoundingClientRect();
                let center_x = (offset.left) + (parseInt(this.width) / 2);
                let center_y = (offset.top) + (parseInt(this.heigth) / 2);
                let player_x = playerObj.offsetLeft + (parseInt(playerObj.style.width) / 2),
                    player_y = playerObj.offsetTop + (parseInt(playerObj.style.height) / 2);

                let ifZombieIsOnCamTop = parseInt(zombie.style.top) >= playerObj.offsetTop;
                let ifZombieIsOnCamBot = parseInt(zombie.style.top) <= playerObj.offsetTop + 700;
                let ifZombieIsOnCamY = ifZombieIsOnCamTop && ifZombieIsOnCamBot;

                let ifZombieIsOnCamLeft = parseInt(zombie.style.left) >= playerObj.offsetLeft;
                let ifZombieIsOnCamRight = parseInt(zombie.style.left) <= playerObj.offsetLeft + 700;
                let ifZombieIsOnCamX = ifZombieIsOnCamLeft && ifZombieIsOnCamRight;

                if (ifZombieIsOnCamY && ifZombieIsOnCamX) {
                    zombie.style.visibility = 'visible';
                } else {
                    zombie.style.visibility = 'hidden';
                }

                if (center_x < player_x) {
                    zombie.style.left = `${parseInt(zombie.style.left) + this.speed}px`;
                } else if (center_x > player_x) {
                    zombie.style.left = `${parseInt(zombie.style.left) - this.speed}px`;
                }
                if (center_y < player_y) {
                    zombie.style.top = `${parseInt(zombie.style.top) + this.speed}px`;
                } else if (center_y > player_y) {
                    zombie.style.top = `${parseInt(zombie.style.top) - this.speed}px`;
                }

                let new_player_x_left = center_x >= player_x - 35;
                let new_player_x_right = center_x <= player_x + 35;
                let collision_x = new_player_x_left && new_player_x_right;

                let new_player_y_top = center_y >= player_y - 35;
                let new_player_y_bot = center_y <= player_y + 35;
                let collision_y = new_player_y_top && new_player_y_bot;

                if (collision_x && collision_y) {
                    if (!player.damaged && player.life > 0) {
                        player.damaged = true;
                        const lifeBubble = document.createElement('span');
                        lifeBubble.setAttribute('class', 'bubble-life')
                        lifeBubble.innerText = `-${this.damage}HP`;
                        lifeBubble.style.color = "red"
                        lifeBubble.style.fontSize = "20px"
                        lifeBubble.style.fontWeight = "bold"
                        lifeBubble.style.position = "absolute";
                        lifeBubble.style.top = `${player_y}px`;
                        lifeBubble.style.left = `${player_x}px`;
                        document.body.append(lifeBubble);
                        player.life -= this.damage;
                        bloodEffect.style.display = 'block';

                        if (player.life === 0) {
                            player.dead();
                        }

                        setTimeout(() => {
                            let arrayBubble = document.querySelectorAll('.bubble-life');
                            bloodEffect.style.display = 'none';
                            arrayBubble.forEach(e => {
                                e.remove();
                            })
                            player.damaged = false;
                        }, 1000);
                    }
                }
            }
        }, 10000)
    }

    collisionBullet(bullet) {
        const zombies = document.querySelectorAll('.zombie');
        const stopwatch = document.getElementById('stopwatch');
        let positionBullet = bullet.getBoundingClientRect();
        if(zombies.length) {
            zombies.forEach(e => {
                let hitX = positionBullet.x >= e.getBoundingClientRect().x && positionBullet.x <= e.getBoundingClientRect().x + 80;
                let hitY = positionBullet.y >= e.getBoundingClientRect().y && positionBullet.y <= e.getBoundingClientRect().y + 80;
                if(hitX && hitY) {
                    e.children[0].value -= 40;
                    bullet.remove();
                    if(e.children[0].value <= 0) {
                        let nbrZombierest = parseInt(localStorage.getItem('nbrzombie'));
                        let nbrZombieMax = parseInt(localStorage.getItem('nbrzombiemax'));
                        e.append(this.zombieDead());
                        nbrZombierest -= 1;
                        localStorage.setItem('nbrzombie', nbrZombierest.toString());
                        stopwatch.style.color = "black";
                        stopwatch.innerText = `${nbrZombierest} zombies restants`;
                        if(nbrZombierest === 0) {
                            this.waveManager = new WaveManager();
                            this.waveManager.goNextStage();
                        }
                        setTimeout(() => {
                            this.dropAmmo(e.getBoundingClientRect().x, e.getBoundingClientRect().y);
                            e.remove();
                        }, 700)
                    }
                }
            });
        }
    }

    dropAmmo(X, Y) {
        let tauxDrop = Math.floor(Math.random() * 3);

        const Pickup = document.createElement('div');
        Pickup.style.width = '50px';
        Pickup.style.height = '50px';
        Pickup.style.position = 'absolute';
        Pickup.style.top = `${parseInt(Y)}px`;
        Pickup.style.left = `${parseInt(X)}px`;
        Pickup.style.backgroundSize = 'contain';
        Pickup.style.backgroundRepeat = 'no-repeat';

        switch (tauxDrop) {
            case 1:
                Pickup.setAttribute('class', 'pickup-ammo')
                Pickup.style.backgroundImage = `url('${this.ammoBox}')`;
                break;
            case 2:
                Pickup.setAttribute('class', 'pickup-heal')
                Pickup.style.backgroundImage = `url('${this.healthBox}')`;
                break;
        }

        const screen = document.getElementById('root');
        screen.append(Pickup);

        setTimeout(() => {
            Pickup.remove();
        }, 15000)
    }
}