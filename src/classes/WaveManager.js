import {Zombie} from "./Zombie";

let actualWave = localStorage.getItem('wave');


export class WaveManager {
    constructor() {
        this.timerMinute = 0;
        this.timerSeconde = 15;
        this.zombieClass = new Zombie();
    }

    initiateWaves() {
        this.stopwatch = document.createElement('span');
        this.stopwatch.setAttribute('id', 'stopwatch')
        this.stopwatch.style.fontSize = "28px";
        this.stopwatch.innerText = `0${this.timerMinute}:${this.timerSeconde}`;

        return this.stopwatch
    }

    timerBeforeStart() {
        this.nbrZombie = localStorage.getItem('nbrzombiemax');
        this.stopwatch = this.stopwatch ? this.stopwatch : document.getElementById('stopwatch');
        setInterval(() => {
            if(this.timerSeconde !== 0) {
                this.timerSeconde--;
                if(this.timerSeconde === 0) {
                    if(this.timerMinute === 0) {
                        this.stopwatch.innerText = `0${this.timerMinute}:0${this.timerSeconde}`;
                        this.zombieClass.spawnZombies(this.nbrZombie * (Math.round(actualWave/2)));
                        localStorage.setItem('nbrzombiemax', (this.nbrZombie * (Math.round(actualWave/2))).toString());
                        localStorage.setItem('nbrzombie', (this.nbrZombie * (Math.round(actualWave/2))).toString());
                        this.stopwatch.style.color = "black";
                        this.stopwatch.innerText = `${(this.nbrZombie * (Math.round(actualWave/2)))} zombies restants`;
                    } else {
                        this.timerMinute--;
                    }
                } else {
                    if (this.timerSeconde > 0 && this.timerSeconde < 10) {
                        this.stopwatch.style.color = 'red';
                        this.stopwatch.innerText = `0${this.timerMinute}:0${this.timerSeconde}`;
                    } else {
                        this.stopwatch.style.color = 'black';
                        this.stopwatch.innerText = `0${this.timerMinute}:${this.timerSeconde}`;
                    }
                }
            }
        }, 500)
    }

    goNextStage() {
        const waveText = document.getElementById('wave-text');
        actualWave ++;
        waveText.innerText = `Vague ${actualWave}`;
        this.timerBeforeStart();
    }
}