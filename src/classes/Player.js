import sprite from '../assets/images/sprite2D/player/soldier.gif';
import bullet from '../assets/images/sprite2D/environnement/bullet.png';
import explosion from '../assets/images/sprite2D/environnement/explosion.png';
import reloading from '../assets/images/sprite2D/player/reloading.gif';
import {WaveManager} from './WaveManager';
import {Zombie} from "./Zombie";

export class Player {
    constructor(pseudo) {
        this.width = "70px";
        this.height = "70px";
        this.sprite = sprite;
        this.spriteBullet = bullet;
        this.animExplo = explosion;
        this.animReload = reloading;
        this.speedFire = 100;
        this.speed = 7;
        this.speedmin = 7;
        this.speedmax = 10;
        this.ammo = 30;
        this.maxAmmo = 60;
        this.totalAmmoMax = 200;
        this.life = 200;
        this.maxLife = 200;
        this.shooting = true;
        this.reloading = false;
        this.bulletPerCharg = 30;
        this.waveManagerClasse = new WaveManager();
        this.zombieClasse = new Zombie();
        this.pseudo = pseudo;
    }

    createPlayer() {
        this.player = document.createElement('div');
        this.player.setAttribute('id', 'player')
        this.player.style.width = this.width;
        this.player.style.height = this.height;
        this.player.style.backgroundImage = `url('${this.sprite}')`;
        this.player.style.backgroundRepeat = 'no-repeat';
        this.player.style.backgroundSize = 'contain';
        this.player.style.transform = 'rotate(-90deg)';

        this.name = document.createElement('span');
        this.name.setAttribute('id', 'pseudoBubble');
        this.name.innerText = this.pseudo;
        this.name.style.padding = '0 5px';
        this.name.style.backgroundColor = 'black';
        this.name.style.color = 'white';
        this.name.style.fontSize = '18px';
        this.name.style.position = 'absolute';
        this.name.style.top = '20px';
        this.name.style.left = '-20px';
        this.name.style.opacity = '0.5';
        this.name.style.transform = 'rotate(90deg)';

        this.player.append(this.name);

        return this.player;
    }

    makeHUD() {
        this.HUD = document.createElement('div');
        this.HUD.style.width = '100%';
        this.HUD.style.height = '100%';

        this.waveManager = document.createElement('div');
        this.waveManager.style.display = 'flex';
        this.waveManager.style.flexDirection = 'column';
        this.waveManager.style.alignItems = 'center';
        this.waveManager.style.justifyContent = 'center';
        this.waveManager.style.padding = '0 50px 20px 50px';
        this.waveManager.style.width = '20%';
        this.waveManager.style.fontSize = '50px';
        this.waveManager.style.color = 'black';
        this.waveManager.style.backgroundColor = 'white';
        this.waveManager.style.borderBottomLeftRadius = '60px';
        this.waveManager.style.borderBottomRightRadius = '60px';
        this.waveManager.style.opacity = '0.8';
        this.waveManager.style.position = 'absolute';
        this.waveManager.style.top = '0';
        this.waveManager.style.left = '40%';

        this.waveText = document.createElement('span');
        this.waveText.setAttribute('id', 'wave-text');
        this.waveText.innerText = `Vague ${localStorage.getItem('wave')}`;

        this.waveManager.append(this.waveText);
        this.waveManager.append(this.waveManagerClasse.initiateWaves());

        this.lifeManager = document.createElement('div');
        this.lifeManager.style.display = 'flex';
        this.lifeManager.style.flexDirection = 'column';
        this.lifeManager.style.alignItems = 'center';
        this.lifeManager.style.justifyContent = 'center';
        this.lifeManager.style.position = 'absolute';
        this.lifeManager.style.backgroundColor = 'white';
        this.lifeManager.style.padding = '20px';
        this.lifeManager.style.borderTopRightRadius = '60px';
        this.lifeManager.style.opacity = '0.8';
        this.lifeManager.style.bottom = '0';
        this.lifeManager.style.left = '0';

        this.lifeText = document.createElement('span');
        this.lifeText.setAttribute('class', 'lifeText');
        this.lifeText.style.fontSize = '27px';
        this.lifeText.innerText = `${this.life}/${this.maxLife} HP`;
        this.lifeText.style.fontWeight = 'bold';
        this.lifeManager.append(this.lifeText);

        this.lifeBar = document.createElement('progress');
        this.lifeBar.setAttribute('class', 'lifeBar');
        this.lifeBar.value = this.life;
        this.lifeBar.max = this.maxLife;
        this.lifeManager.append(this.lifeBar);

        this.ammoManager = document.createElement('div');
        this.ammoManager.setAttribute('id', 'ammoManager')
        this.ammoManager.style.display = 'flex';
        this.ammoManager.style.flexDirection = 'column';
        this.ammoManager.style.alignItems = 'center';
        this.ammoManager.style.justifyContent = 'center';
        this.ammoManager.style.position = 'absolute';
        this.ammoManager.style.backgroundColor = 'white';
        this.ammoManager.style.padding = '20px';
        this.ammoManager.style.borderTopLeftRadius = '60px';
        this.ammoManager.style.opacity = '0.8';
        this.ammoManager.style.bottom = '0';
        this.ammoManager.style.right = '0';

        this.ammoText = document.createElement('span');
        this.ammoText.setAttribute('class', 'ammoText');
        this.ammoText.style.fontSize = '27px';
        this.ammoText.innerText = `${this.ammo}/${this.maxAmmo} Mun`;
        this.ammoText.style.fontWeight = 'bold';
        this.ammoManager.append(this.ammoText);

        this.ammoBar = document.createElement('progress');
        this.ammoBar.setAttribute('class', 'ammoBar');
        this.ammoBar.value = this.ammo;
        this.ammoBar.max = 30;
        this.ammoManager.append(this.ammoBar);

        this.HUD.append(this.waveManager);
        this.HUD.append(this.lifeManager);
        this.HUD.append(this.ammoManager);

        this.waveManagerClasse.timerBeforeStart();

        return this.HUD
    }

    reloadHUD() {
        this.ammoText.innerText = `${this.ammo}/${this.maxAmmo} Mun`;
        this.ammoBar.value = this.ammo;
        this.ammoBar.max = 30;

        this.lifeText.innerText = `${this.life}/${this.maxLife} HP`;
        this.lifeBar.value = this.life;
        this.lifeBar.max = this.maxLife;
    }

    followCursor(e) {
        let offset = this.player.getBoundingClientRect();
        let center_x = (offset.left) + (parseInt(this.width)/2);
        let center_y = (offset.top) + (parseInt(this.height)/2);
        let mouse_x = e.pageX, mouse_y = e.pageY;
        let radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
        let degree = (radians * (180 / Math.PI) * -1) + 90;
        this.player.style.transform = `rotate(${degree}deg)`;
    }

    shoot(e) {
        this.speed = this.speedmin;
        if(this.ammo > 0) {
            let bullets = document.querySelectorAll('.bullets');
            bullets.forEach(e => {
               e.remove();
            });
            let camera = document.getElementById('camera')
            if(!this.reloading) {
                this.animShoot = document.createElement('div')
                this.animShoot.style.width = '30px';
                this.animShoot.style.height = '30px';
                this.animShoot.style.borderRadius = '100%';
                this.animShoot.style.backgroundImage = `url('${this.animExplo}')`;
                this.animShoot.style.backgroundSize = 'contain';
                this.animShoot.style.backgroundRepeat = 'no-repeat';
                this.animShoot.style.position = 'absolute';
                this.animShoot.style.opacity = '0.8';
                this.animShoot.style.top = '45%';
                this.animShoot.style.left = '50px';

                this.bullet = document.createElement('div');
                this.bullet.setAttribute('class', 'bullets');
                this.bullet.style.width = '50px';
                this.bullet.style.height = '50px';
                this.bullet.style.backgroundImage = `url('${this.spriteBullet}')`;
                this.bullet.style.backgroundSize = 'contain';
                this.bullet.style.backgroundRepeat = 'no-repeat';
                this.bullet.style.position = 'absolute';
                this.bullet.style.top = '35%';
                this.bullet.style.left = '50px';

                this.player.append(this.animShoot);
                setTimeout(() => {
                    this.animShoot.remove();
                }, 120)
                this.player.append(this.bullet);
                this.ammo -= 1;
                const shootInterv = setInterval(() => {
                    this.bullet.style.left = `${parseInt(this.bullet.style.left) + this.speedFire}px`;
                    this.zombieClasse.collisionBullet(this.bullet);
                    if (parseInt(this.bullet.style.left) > 2000) {
                        this.bullet.remove();
                        clearInterval(shootInterv);
                    }
                }, 60)
                this.shooting = false;

                setTimeout(() => {
                    this.shooting = true;
                }, 500)
            }
        } else if(this.ammo === 0 && this.maxAmmo > 0) {
            this.reload();
        } else {
            this.outOfAmmo = document.createElement('span');
            this.outOfAmmo.setAttribute('class', 'ooa');
            this.outOfAmmo.style.fontSize = '22px';
            this.outOfAmmo.style.fontWeight = 'bold';
            this.outOfAmmo.style.width = '120px';
            this.outOfAmmo.style.padding = '10px';
            this.outOfAmmo.style.transform = 'rotate(90deg)';
            this.outOfAmmo.style.position = 'absolute';
            this.outOfAmmo.style.top = '50%';
            this.outOfAmmo.style.left = '50%';
            this.outOfAmmo.innerText = 'Out of ammo !';
            this.player.append(this.outOfAmmo);
            this.ammoManager.style.backgroundColor = 'crimson';

            setTimeout(() => {
                let allOoa = document.querySelectorAll('.ooa');
                allOoa.forEach(e => {
                    e.remove();
                })
                this.ammoManager.style.backgroundColor = 'white';
            }, 300)
        }
    }

    action(keycode, type) {
        const camera = document.getElementById('camera');
        const rayonCamera = 340;
        const windowWidth = window.innerWidth;
        const windowHeight = window.innerHeight;
        switch (keycode) {
            // touche "Z"
            case 90:
                if((parseInt(camera.style.top) + rayonCamera) > 0) {
                    camera.style.top = `${parseInt(camera.style.top) - this.speed}px`;
                    this.walkOnHealOrAmmo();
                }
                break;
            // touche "D"
            case 68:
                if(((parseInt(camera.style.left) + parseInt(camera.style.width)) - rayonCamera) < windowWidth) {
                    camera.style.left = `${parseInt(camera.style.left) + this.speed}px`;
                    this.walkOnHealOrAmmo();
                }
                break;
            // touche "S"
            case 83:
                if(((parseInt(camera.style.top) + parseInt(camera.style.height)) - rayonCamera) < windowHeight) {
                    camera.style.top = `${parseInt(camera.style.top) + this.speed}px`;
                    this.walkOnHealOrAmmo();
                }
                break;
            // touche "Q"
            case 81:
                if((parseInt(camera.style.left) + rayonCamera) > 0) {
                    camera.style.left = `${parseInt(camera.style.left) - this.speed}px`;
                    this.walkOnHealOrAmmo();
                }
                break;
            //reloading
            case 82:
                if(type === 'keydown' && !this.reloading) {
                    this.reload();
                }
                break;
            //sprint
            case 16:
                if(type === 'keydown') {
                    this.speed = this.speedmax;
                } else if (type === 'keyup') {
                    this.speed = this.speedmin;
                }
                break;
        }
    }

    reload() {
        if(!this.reloading) {
            this.reloading = true;

            this.reloadingAnim = document.createElement('div');
            this.reloadingAnim.style.width = '8r0px';
            this.reloadingAnim.style.height = '80px';
            this.reloadingAnim.style.top = '50px';
            this.reloadingAnim.style.backgroundImage = `url('${this.animReload}')`;
            this.reloadingAnim.style.backgroundRepeat = 'no-repeat';
            this.reloadingAnim.style.backgroundSize = 'contain';

            this.reloadingText = document.createElement('span');
            this.reloadingText.innerText = 'Reloading !';
            this.reloadingText.style.fontSize = '14px';
            this.reloadingText.style.transform = 'rotate(90deg)';
            this.reloadingText.style.position = 'absolute';
            this.reloadingText.style.top = '30%';
            this.reloadingText.style.color = 'white';

            if(this.maxAmmo > 0 && this.ammo < 30) {
                this.reloadingAnim.append(this.reloadingText);
                this.player.append(this.reloadingAnim);
            }
            setTimeout(() => {
                if(this.maxAmmo > 0 && this.ammo < this.bulletPerCharg) {
                    this.reloading = false;
                    let ballmissed = this.bulletPerCharg - this.ammo;
                    this.ammo = this.bulletPerCharg;
                    this.maxAmmo = this.maxAmmo - ballmissed;
                    this.reloadingAnim.remove();
                }
            }, 1500)
        }
    }

    walkOnHealOrAmmo() {
        const healthPickup = document.querySelectorAll('.pickup-heal');
        const ammoPickup = document.querySelectorAll('.pickup-ammo');

        const Bubble = document.createElement('span');
        Bubble.setAttribute('class', 'bubble-life');
        Bubble.style.fontSize = "20px"
        Bubble.style.fontWeight = "bold"
        Bubble.style.position = "absolute";

        healthPickup.forEach(e => {
            let left_pickup_x = e.getBoundingClientRect().x;
            let top_pickup_y = e.getBoundingClientRect().y;
            let right_pickup_x = e.getBoundingClientRect().x + parseInt(e.style.width);
            let bottom_pickup_y = e.getBoundingClientRect().y + parseInt(e.style.height);
            let center_player_x = this.player.getBoundingClientRect().x + parseInt(this.player.style.width)/2;
            let center_player_y = this.player.getBoundingClientRect().y + parseInt(this.player.style.height)/2;

            let hitX = center_player_x >= left_pickup_x && center_player_x <= right_pickup_x;
            let hitY = center_player_y >= top_pickup_y && center_player_y <= bottom_pickup_y;

            if(hitY && hitX) {
                if(this.life < this.maxLife) {
                    this.life += 10;
                    Bubble.style.top = `${center_player_y}px`;
                    Bubble.style.left = `${center_player_x}px`;
                    Bubble.innerText = `+10HP`;
                    Bubble.style.color = "green";
                    document.body.append(Bubble);

                    setTimeout(() => {
                        Bubble.remove();
                    }, 1000);
                }
                e.remove();
            }
        })

        ammoPickup.forEach(e => {
            let left_pickup_x = e.getBoundingClientRect().x;
            let top_pickup_y = e.getBoundingClientRect().y;
            let right_pickup_x = e.getBoundingClientRect().x + parseInt(e.style.width);
            let bottom_pickup_y = e.getBoundingClientRect().y + parseInt(e.style.height);
            let center_player_x = this.player.getBoundingClientRect().x + parseInt(this.player.style.width)/2;
            let center_player_y = this.player.getBoundingClientRect().y + parseInt(this.player.style.height)/2;

            let hitX = center_player_x >= left_pickup_x && center_player_x <= right_pickup_x;
            let hitY = center_player_y >= top_pickup_y && center_player_y <= bottom_pickup_y;

            if(hitY && hitX) {
                if(this.maxAmmo < this.totalAmmoMax) {
                    this.maxAmmo += 10;
                    Bubble.style.top = `${center_player_y}px`;
                    Bubble.style.left = `${center_player_x}px`;
                    Bubble.innerText = `+10MUN`;
                    Bubble.style.color = "blue";
                    document.body.append(Bubble);
                    setTimeout(() => {
                        Bubble.remove();
                    }, 1000);
                }
                e.remove();
            }
        })
    }

    dead() {
        const menu = document.getElementById('menu');
        const root = document.getElementById('root');

        root.style.display = 'none';
        menu.style.display = 'block';

        menu.innerHTML = "<h1>Vous-êtes mort...</h1>";
    }
}