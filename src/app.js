import './css/style.css';
import {map, camera, light, bloodEffect} from "./components/environnment";
import {Player} from "./classes/Player";
import {Zombie} from "./classes/Zombie";
import bgMenu from '../src/assets/images/background/menu_fond.gif';

const menu = document.getElementById('menu');
const playBtn = document.getElementById('play-btn');
const root = document.getElementById('root');
const pseudoInput = document.getElementById('pseudo-input');
menu.style.backgroundImage = `url('${bgMenu}')`;

playBtn.addEventListener('click', () => {
    let pseudo = pseudoInput.value;
    menu.style.display = 'none';
    root.style.display = 'block';
    startGame(pseudo);
})

playBtn.addEventListener('mouseenter', () => {
    playBtn.innerText = 'Noooo !'
});

playBtn.addEventListener('mouseleave', () => {
    playBtn.innerText = 'Play'
});

const startGame = (pseudo) => {
    localStorage.setItem('wave', '1');
    localStorage.setItem('nbrzombie', '2');
    localStorage.setItem('nbrzombiemax', '2');

    const player = new Player(pseudo);
    const zombie = new Zombie();

    root.append(map);
    root.append(camera);
    root.append(player.makeHUD());
    root.append(bloodEffect);
    camera.append(light);
    camera.append(player.createPlayer());

    document.addEventListener('keydown', e => {
        player.action(e.keyCode, 'keydown');
    })
    document.addEventListener('keyup', e => {
        player.action(e.keyCode, 'keyup');
    })
    document.addEventListener('keypress', e => {
        player.action(e.keyCode, 'keypress');
    })
    document.addEventListener('mousemove', e => {
        player.followCursor(e);
    })
    document.addEventListener('mousedown', e => {
        if(player.shooting) {
            player.shoot(e)
        }
    })
    setInterval(() => {
        let zombiePoped = document.querySelectorAll('.zombie');
        player.reloadHUD();
        if(zombiePoped.length) {
            zombiePoped.forEach(zombieSelected => {
                zombie.followPlayer(zombieSelected);
                zombie.walk(player, zombieSelected);
            })
        }
    }, 60)
}