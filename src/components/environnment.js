import grass from '../assets/images/sprite2D/environnement/grass.png';
import tree1 from '../assets/images/sprite2D/environnement/tree_1.png';
import tree2 from '../assets/images/sprite2D/environnement/tree_2.png';
import blood from '../assets/images/sprite2D/environnement/bloodEffect.png';

const treesPosition = [
    {left: -10, top:5, type:tree1},
    {left: 100, top:50, type:tree2},
    {left: 365, top:678, type:tree1},
    {left: 50, top:400, type:tree1},
    {left: 1000, top:567, type:tree2},
    {left: 540, top:123, type:tree1},
    {left: 687, top:10, type:tree2},
    {left: -10, top:5, type:tree1},
    {left: 1234, top:50, type:tree2},
    {left: 1250, top:370, type:tree1},
    {left: 345, top:394, type:tree2},
    {left: 545, top:345, type:tree1},
    {left: 1000, top:567, type:tree2},
    {left: 1080, top:345, type:tree1},
    {left: 687, top:687, type:tree2},
];

const map = document.createElement('div');
map.setAttribute('id', 'map')
map.style.display = 'flex';
map.style.justifyContent = 'center';
map.style.alignItems = 'center';
map.style.width = '100vw';
map.style.height = '100vh';
map.style.backgroundImage = `url('${grass}')`;
map.style.filter = 'grayscale(80%)';

const camera = document.createElement('div');
camera.setAttribute('id', 'camera')
camera.style.display = 'flex';
camera.style.justifyContent = 'center';
camera.style.alignItems = 'center';
camera.style.width = '700px';
camera.style.height = '700px';
camera.style.position = 'absolute';
camera.style.top = '100px';
camera.style.left = '500px';

const light = document.createElement('div');
light.setAttribute('id', 'light')
light.style.width = '100%';
light.style.height = '100%';
light.style.borderRadius = '100%';
light.style.position = 'absolute';

const bloodEffect = document.createElement('div');
bloodEffect.setAttribute('id', 'bloodEffect')
bloodEffect.style.width = '100vw';
bloodEffect.style.height = '100vh';
bloodEffect.style.position = 'absolute';
bloodEffect.style.top = '0';
bloodEffect.style.left = '0';
bloodEffect.style.opacity = '0.4';
bloodEffect.style.display = 'none';
bloodEffect.style.backgroundImage = `url('${blood}')`;
bloodEffect.style.backgroundRepeat = 'no-repeat';
bloodEffect.style.backgroundSize = 'cover';

const treeSection = document.createElement('div');
treeSection.setAttribute('id', 'tree-section')
treeSection.style.width = '100%';
treeSection.style.height = '100%';
treeSection.style.position = 'absolute';
treeSection.style.top = '0';
treeSection.style.left = '0';

const zombieSection = document.createElement('div');
zombieSection.setAttribute('id', 'zombie-section')
zombieSection.style.width = '100%';
zombieSection.style.height = '100%';
zombieSection.style.position = 'absolute';
zombieSection.style.top = '0';
zombieSection.style.left = '0';

map.append(zombieSection);
map.append(treeSection);

treesPosition.forEach(e => {
    const imgTree = new Image();
    imgTree.setAttribute('draggable', 'false');
    imgTree.src = e.type
    imgTree.style.position = 'absolute';
    imgTree.style.top = `${e.top}px`;
    imgTree.style.left = `${e.left}px`;
    treeSection.append(imgTree);
})

export {map, camera, light, bloodEffect};